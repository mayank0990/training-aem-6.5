package com.aem.training.site.core.servlets;

import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.models.export.spi.ModelExporter;
import org.apache.sling.models.factory.ExportException;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Component(service = Servlet.class, immediate = true,
        property = {
                ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_GET,
                ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/test"
        })
@Designate(ocd = TestServlet.Config.class)
public class TestServlet extends SlingSafeMethodsServlet {

    private Logger logger = LoggerFactory.getLogger(TestServlet.class);

    // Servlet OSGi Configuration
    @ObjectClassDefinition(name = "Test Servlet Configs", description = "")
    @interface Config {
        @AttributeDefinition(name = "Project ID", description = "Mention the Project ID")
        String project_id() default "Test Servlet ID";
    }

    @Activate
    private Config config;

    // To not Serialize this object, use transient keyword
    @Reference
    private transient ModelExporter modelExporter;

    @Reference
    private transient ResourceResolverFactory resourceResolverFactory;

    @Reference
    private transient Replicator replicator;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        String param = request.getParameter("prop");
        String paramVal = request.getParameter("propVal");
        ResourceResolver resourceResolver = null;

        /**
         * If you're working in Author Instance, then Resource Resolver from Request
         * will give you the "admin" session, hence you will be able to do CRUD operations in JCR
         * But, if you're doing the same for Publish Instance, you will get the Resource Resolver
         * of "Anonymous" User who has only "READ" access and you won't be able to do CRUD operations.
         *
         * For Author Instance, it's okay to get the Resource Resolver from Request,
         * But, in Publish Instance, Always make sure if you want to perform CRUD Operations,
         * then you get the Resource resolver from SERVICE USER with the help of Resource Resolver Factory
         * */

        // Via Request - it will give you logged in User Resolver
        //resourceResolver = request.getResourceResolver();
        //Resource testResource = resourceResolver.getResource("/content/test");

        // Via Service User
        // Steps to Create Service User in AEM
        // 1. Go to http://localhost:4502/crx/explorer/index.jsp
        // 2. First Check, if you're logged in or not
        // 3. Click on User Administration
        // 4. Click on Create System User & give the UserID
        // 5. Click on Green Tick to Save the changes.
        // 6. Go to http://localhost:4502/useradmin to check the user
        // 7. Always give the SPECIFIC Permissions to the System User
        // 8. Go to http://localhost:4502/system/console/configMgr
        // 9. Look for the OSGi Config "User Mapper Service Amendment"
        // 10. In Service Mapping, put the user mapping for your service user
        // 11. Format for User Mapping: "bundleId ":" [subServiceName] "=" [userName]"

        // This is the way to get the Resource Resolver via Service User
        final Map<String, Object> authInfo = new HashMap<>();

        // Name of the SubService (IT IS DIFFERENT FROM SERVICE USER)
        authInfo.put(ResourceResolverFactory.SUBSERVICE, "readContentSubService");
        try {
            // Resolver Factory try to login with this Service User in AEM Env and it does
            // with the help of the "User Mapper Service Amendment"
            // If it fails to login, then it throws LoginException
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(authInfo);
        } catch (LoginException ignored) {
        }

        Resource testResource = null;
        // This Resource Resolver is extracted via Service User
        if (null != resourceResolver) {
            testResource = resourceResolver.getResource("/content/test");
        }

        // Get the Node from Resolver
        if (null != testResource) {
            try {
                // CRUD Operations
                // There are 2 types of ValueMaps
                // 1. ValueMap - It is only used for GET the values from NODE
                // 2. ModifiableValueMap - It is used for both GET AND ADD values in NODE
                ModifiableValueMap modifiableValueMap = testResource.adaptTo(ModifiableValueMap.class);
                if (null != modifiableValueMap && StringUtils.isNotBlank(param) && StringUtils.isNotBlank(paramVal)) {

                    // Add Property Operation
                    modifiableValueMap.put(param, paramVal);

                    // To Save the Changes
                    resourceResolver.commit();
                }

                // We are using ModelExporter Service to Convert the Resource into JSON
                String nodeJson = modelExporter.export(testResource, String.class, new HashMap<>());

                // We are converting a String JSON to JSON OBJECT to iterate the items
                JSONObject jsonObject = new JSONObject(nodeJson);
                JSONObject responseObj = new JSONObject();
                Iterator<?> iterator = jsonObject.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    if (jsonObject.has(key) && !key.startsWith("jcr") && !key.startsWith("cq")) {
                        responseObj.put(key, jsonObject.get(key));
                        responseObj.put("Resolver-User-ID", resourceResolver.getUserID());
                        responseObj.put("Config-ID", config.project_id());
                    }
                }


                // Replicate Page using Replicator API
                replicator.replicate(resourceResolver.adaptTo(Session.class),
                        ReplicationActionType.ACTIVATE, testResource.getPath());

                // After using the ResourceResolver in your code, Make sure you always CLOSE the Resolver Object
                // After you have closed the ResourceResolver, You cannot access any of it's methods.
                // They all will give you NULL
                if (resourceResolver.isLive()) {
                    resourceResolver.close();
                }


                // Sending the Response to the Calling Client
                out.println(responseObj);
                response.flushBuffer();
            } catch (ExportException | JSONException | ReplicationException e) {
                logger.error("Exception: {}", e);
            }
        }
    }
}
