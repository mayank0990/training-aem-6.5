package com.aem.training.site.core.workflows;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Value;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(service = WorkflowProcess.class,
        property = {
                "process.label" + "=" + "Test Process"
        })
public class CustomProcessStep implements WorkflowProcess {

    private static Logger logger = LoggerFactory.getLogger(CustomProcessStep.class);

    private static final String PN_USER_EMAIL = "profile/email";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public void execute(WorkItem workItem, WorkflowSession workflowSession,
                        MetaDataMap metaDataMap) throws WorkflowException {
        String workflowInitiator = workItem.getWorkflow().getInitiator();
        String workflowPayload = workItem.getWorkflowData().getPayload().toString();
        logger.info("Workflow Initiator: {}", workflowInitiator);
        logger.info("Workflow Payload: {}", workflowPayload);

        // If a workflow fails, then it tries max number of 10 times
        // It can be checked in the "Granite Workflow Queue"

        // Send Email http://<domain>/editor.html/payloadPath
        // In the Email, Send the Comment added by Approver

        // 1. Get the Initiator's EMAIL ID
        // 2. Get the Approver's Comment at the time of Rejection


        ResourceResolver resourceResolver = null;
        final Map<String, Object> authInfo = new HashMap<>();
        authInfo.put(ResourceResolverFactory.SUBSERVICE, "readContentSubService");
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(authInfo);
        } catch (LoginException ignored) {
        }

        // There's an API UserManager which helps us to find the Users/Authorizables
        UserManager userManager = resourceResolver.adaptTo(UserManager.class);
        try {
            Authorizable auth = userManager.getAuthorizable(workflowInitiator);
            String email = getAuthorizableEmail(auth);
            logger.info("User EMAIL: {}", email);
        } catch (RepositoryException ignored) {
        }

        // 2. Get the comment
        List<HistoryItem> historyItemList = workflowSession.getHistory(workItem.getWorkflow());
        HistoryItem lastItem = historyItemList.get(historyItemList.size() - 1);
        if (null != lastItem) {
            String comment = lastItem.getComment();
            logger.info("User Comment: {}", comment);
        }

        // Inject EMAIL Service and then send the EMAIL
    }


    private static String getAuthorizableEmail(Authorizable authorizable) throws RepositoryException {
        if (authorizable.hasProperty(PN_USER_EMAIL)) {
            Value[] emailVal = authorizable.getProperty(PN_USER_EMAIL);
            return emailVal[0].getString();
        }
        return null;
    }
}
